Set userid and groupid of container with environment vars
PUID=<uid(userid)>
PGID=<gid(groupid)>

Remove (leave container in timezone UTC) or edit files/etc/cont-init.d/11-timezone to your preference


S6-Overlay Version v1.21.4.0 (https://github.com/just-containers/s6-overlay)
Current Alpine Version 3.7.0 (Released Nov 30, 2017)
