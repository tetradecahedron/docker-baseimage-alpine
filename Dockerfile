FROM scratch
ADD alpine-minirootfs-3.7.0-x86_64.tar.gz /
LABEL maintainer="tetradecahedron"

# Copy local files to container
COPY files/ /

# Environment variables
ENV PS1="$(whoami)@$(hostname):$(pwd)$ " \
HOME="/root" \
TERM="xterm"

# Install and configure base utilities 
RUN apk add --no-cache \
		tar \
		bash \
		nano \
		ca-certificates \
		coreutils \
		shadow \
		tzdata && \
	groupmod -g 1000 users && \
	useradd -u 911 -U -d /config -s /bin/false abc && \
	usermod -G users abc &&\
	mkdir -p \
		/config \
		/defaults && \
	apk upgrade --no-cache && \
	tar xzf /s6-overlay-amd64.tar.gz -C / && \
	rm /s6-overlay-amd64.tar.gz

WORKDIR /tmp

ENTRYPOINT ["/init"]
